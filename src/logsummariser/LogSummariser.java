package logsummariser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kurtis Saddler
 */
public class LogSummariser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        summariseData(args[0], args[1]);
    }
    
    private static void summariseData(String inputFile, String outputFile) {
        // = null;
        try {
            List<String> data = readDataFile(inputFile);
            List<String> report = createSummary(data);
            writeReportFile(report, outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
     *  Read the contents of a CSV file and pass useful data into a list of strings
     */
    private static List<String> readDataFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line;
        List<String> extractedData = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            String[] data = line.split(",");

            extractedData.add(data[2] + "," + data[13]);
        }

        return extractedData;
    }

    /*
     *  Build summary report using filtered data
     */
    private static List<String> createSummary(List<String> filteredData) {
        List<String> summary = new ArrayList<>();
        summary.add("Label, Count, Average");
        
        // Ignore the header line of the log file
        for (int i = 1; i < filteredData.size(); i++) {
            String[] data = filteredData.get(i).split(",");

            String label = data[0];
            int count = 1;
            int sumTotalLatency = Integer.valueOf(data[1]);

            for (int j = (i + 1); j < filteredData.size(); j++) {
                String[] comparison = filteredData.get(j).split(",");

                if (label.equals(comparison[0])) {
                    count++;
                    sumTotalLatency += Integer.valueOf(comparison[1]);
                    filteredData.remove(j);
                }
            }

            summary.add(label + ", " + count + ", " + (sumTotalLatency/count));
        }

        return summary;
    }

    /*
     *  Write the contents of the summary list to a new file
     */
    private static void writeReportFile(List<String> data, String outputFile) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));

        for (String str: data) {
            bw.write(str);
            bw.newLine();
        }

        bw.close();
    }
    
}
